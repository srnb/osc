package xyz.degreetech.o

import xyz.degreetech.o.buildinfo.BuildInfo
import xyz.degreetech.o.impl.VersionedProtocol
import xyz.degreetech.o.impl.VersionedProtocol.Version
import xyz.degreetech.o.impl.GitVersion

object OProtocolVersion {

  def apply(): VersionedProtocol =
    VersionedProtocol("o", "https://gitlab.com/degreetech/o", Version.GitVersion(GitVersion(BuildInfo.oVersion)))

}
