import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}
import scalapb.compiler.Version.scalapbVersion
import sys.process._

lazy val protoSource = settingKey[File]("Where the ProtoBuf source is")

val publishSettings = Seq(
  organization         := "tf.bug",
  organizationName     := "bugtf",
  organizationHomepage := Some(url("https://bug.tf/")),
  scmInfo := Some(
    ScmInfo(
      url("https://gitlab.com/srnb/osc"),
      "scm:git@gitlab.com:srnb/osc.git"
    )
  ),
  developers := List(
    Developer(
      id = "srnb",
      name = "Anthony Cerruti",
      email = "me@s5.pm",
      url = url("https://s5.pm")
    )
  ),
  description := "https://gitlab.com/degreetech/o Scala Bindings",
  licenses    := List("GPLv3" -> new URL("https://www.gnu.org/licenses/gpl-3.0.txt")),
  homepage    := Some(url("https://gitlab.com/srnb/osc")),
  // Remove all additional repository other than Maven Central from POM
  pomIncludeRepository := { _ =>
    false
  },
  publishTo := {
    val nexus = "https://oss.sonatype.org/"
    if (isSnapshot.value) Some("snapshots" at nexus + "content/repositories/snapshots")
    else Some("releases" at nexus + "service/local/staging/deploy/maven2")
  },
  publishMavenStyle    := true,
  publishConfiguration := publishConfiguration.value.withOverwrite(true),
)

val sharedSettings = Seq(
  organization := "tf.bug",
  name         := "osc",
  version      := "1.0.0",
  scalaVersion := "2.11.12",
  libraryDependencies ++= Seq(
    "com.thesamet.scalapb" %%% "scalapb-runtime" % scalapbVersion,
    "com.thesamet.scalapb" %% "scalapb-runtime"  % scalapbVersion % "protobuf"
  ),
  PB.targets in Compile := Seq(
    scalapb.gen() -> (sourceManaged in Compile).value / "protos",
  ),
  protoSource                := ((baseDirectory in ThisBuild).value / "osc" / "src" / "main" / "protobuf"),
  PB.protoSources in Compile := Seq(protoSource.value),
  buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion, BuildInfoKey.action("oVersion") {
    (Process("git rev-list --all --count", protoSource.value) !!).trim.toInt
  }),
  buildInfoPackage := "xyz.degreetech.o.buildinfo",
) ++ publishSettings

lazy val osc = crossProject(JSPlatform, JVMPlatform, NativePlatform)
  .crossType(CrossType.Pure)
  .enablePlugins(BuildInfoPlugin)
  .settings(sharedSettings)
  .jsSettings(crossScalaVersions := Seq("2.11.12", "2.12.8"))
  .jvmSettings(crossScalaVersions := Seq("2.10.7", "2.11.12", "2.12.8"))
  .nativeSettings(crossScalaVersions := Seq("2.11.12"))

lazy val oscJS = osc.js
lazy val oscJVM = osc.jvm
lazy val oscNative = osc.native
